# CarResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**plate** | **string** | Car plate | [optional] 
**model** | **string** | Car model | [optional] 
**brand** | **string** | Car brand | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



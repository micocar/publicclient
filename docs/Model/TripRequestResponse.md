# TripRequestResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **int** | Trip request identifier | [optional] 
**status** | [**\Swagger\Client\Model\TripStatusResponse**](TripStatusResponse.md) |  | [optional] 
**time_events** | [**\Swagger\Client\Model\TimeEventsResponse**](TimeEventsResponse.md) |  | [optional] 
**driver** | [**\Swagger\Client\Model\DriverResponse**](DriverResponse.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# OptionsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pay_with_card** | **bool** | Whether is set to pay with card or not on the user preferences | [optional] 
**use_wallet** | **bool** | Whether is set to apply the discounts acumulated on the user wallet or not | [optional] 
**card_type** | **int** | The card selected type:       &lt;ul&gt;       &lt;li&gt;0: Personal card&lt;/li&gt;       &lt;li&gt;1: Company card (autonomous)&lt;/li&gt;       &lt;li&gt;2: Company card (enterprise)&lt;/li&gt;       &lt;/ul&gt; | [optional] 
**company_role** | **string** | Company role       &lt;ul&gt;       &lt;li&gt;0: normal user&lt;/li&gt;       &lt;li&gt;1: admin&lt;/li&gt;       &lt;/ul&gt; | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# TripStatusResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **int** | Status of the trip, there are several possible values in regard of the current state of the trip:      &lt;ul&gt;      &lt;li&gt;0: NONE&lt;/li&gt;      &lt;li&gt;1: REQUESTED&lt;small&gt; Trip requested and waiting for some driver to accept it&lt;/small&gt;&lt;/li&gt;      &lt;li&gt;2: ACCEPTED&lt;small&gt; Driver accepted, will Start inmediatelly&lt;/small&gt;&lt;/li&gt;      &lt;li&gt;3: REJECTED&lt;small&gt; The driver rejected the request&lt;/small&gt;&lt;/li&gt;      &lt;li&gt;4: CANCELLED_BEFORE_ASSIGNING&lt;small&gt; The passenger cancelled the trip before any driver had accepted&lt;/small&gt;&lt;/li&gt;      &lt;li&gt;6: ACCEPTED_BY_ANOTHER_DRIVER&lt;/li&gt;      &lt;li&gt;7: CANCELLED_BY_DRIVER&lt;/li&gt;      &lt;li&gt;8: CANCELLED_BY_PASSENGER&lt;small&gt; The passenger cancelled the trip&lt;/small&gt;&lt;/li&gt;      &lt;li&gt;10: STARTED&lt;small&gt; The trip was accepted by some driver and has started&lt;/small&gt;&lt;/li&gt;      &lt;li&gt;11: ENDED_UNPAID&lt;small&gt; Trip ended but some error happened with the payment (ej: no founds on the credit card)&lt;/small&gt;&lt;/li&gt;      &lt;li&gt;12: ENDED_PAID&lt;/li&gt;      &lt;li&gt;15: ENDED&lt;/li&gt;      &lt;/ul&gt; | [optional] 
**description** | **string** | Trip status description | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



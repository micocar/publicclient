# InitialDataResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version_app** | **string** |  | [optional] 
**city_radio** | **string** |  | [optional] 
**fake_taxi** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



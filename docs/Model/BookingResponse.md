# BookingResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**booking_id** | **int** | Booking identifier | [optional] 
**distance** | **int** | Estimated distance from origin to destination, meters | [optional] 
**time** | **int** | Estimated time from origin to destination, seconds | [optional] 
**finished** | **bool** | Whether the booking service has been finished or not | [optional] 
**cancelled** | **bool** | Whether the booking service has been cancelled or not | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



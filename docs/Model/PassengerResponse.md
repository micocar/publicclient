# PassengerResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | User identifier | [optional] 
**name** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**company** | [**\Swagger\Client\Model\CompanyResponse**](CompanyResponse.md) |  | [optional] 
**options** | [**\Swagger\Client\Model\OptionsResponse**](OptionsResponse.md) |  | [optional] 
**session_token** | **string** | User session token | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



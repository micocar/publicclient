# CompanyResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Company identifier | [optional] 
**name** | **string** |  | [optional] 
**location** | [**\Swagger\Client\Model\LocationResponse**](LocationResponse.md) |  | [optional] 
**cif** | **string** |  | [optional] 
**code** | **string** | Company referal code | [optional] 
**main_user_id** | **int** | Main admin user of the company, will have access to special features on the panel | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



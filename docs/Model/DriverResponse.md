# DriverResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Driver identifier | [optional] 
**name** | **string** | First name | [optional] 
**last_name** | **string** | Last name | [optional] 
**phone** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**car** | [**\Swagger\Client\Model\CarResponse**](CarResponse.md) |  | [optional] 
**position** | [**\Swagger\Client\Model\PositionResponse**](PositionResponse.md) |  | [optional] 
**session_token** | **string** | User session token | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



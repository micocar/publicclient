# TimeEventsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requested** | **string** | The trip was requested at | [optional] 
**ended** | **string** | The trip was ended at | [optional] 
**accepted** | **string** | The trip was accepted at | [optional] 
**cancelled** | **string** | The trip was cancelled at | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



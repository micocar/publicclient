# UserResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | User identifier | [optional] 
**session_token** | **string** | User session token | [optional] 
**name** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**email** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



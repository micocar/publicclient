# Swagger\Client\UserApi

All URIs are relative to *http://localhost:8000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getInitialData**](UserApi.md#getInitialData) | **GET** /api/{version}/users/{id}/get_initial_data | Returns some useful data for when user is just logged


# **getInitialData**
> \Swagger\Client\Model\InitialDataResponse getInitialData($session_token, $version, $id)

Returns some useful data for when user is just logged

Device apps need some starting data like - versionApp - cityRadio - fakeTaxi

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('X-API-KEY', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-KEY', 'Bearer');
// Configure API key authorization: appId
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('X-APP-ID', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-APP-ID', 'Bearer');

$apiInstance = new Swagger\Client\Api\UserApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$session_token = "session_token_example"; // string | User session token
$version = "version_example"; // string | 
$id = "id_example"; // string | 

try {
    $result = $apiInstance->getInitialData($session_token, $version, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->getInitialData: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_token** | **string**| User session token |
 **version** | **string**|  |
 **id** | **string**|  |

### Return type

[**\Swagger\Client\Model\InitialDataResponse**](../Model/InitialDataResponse.md)

### Authorization

[apiKey](../../README.md#apiKey), [appId](../../README.md#appId)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


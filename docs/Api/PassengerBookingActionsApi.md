# Swagger\Client\PassengerBookingActionsApi

All URIs are relative to *http://localhost:8000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancelBooking**](PassengerBookingActionsApi.md#cancelBooking) | **GET** /api/public/{version}/booking/{bookingId}/passenger/{id}/cancel_booking | Passenger cancels a booking
[**requestBooking**](PassengerBookingActionsApi.md#requestBooking) | **GET** /api/public/{version}/booking/passenger/{id}/request_booking | Passenger requests a booking, this will eventually register a trip but with a meeting datetime specified explicitely


# **cancelBooking**
> \Swagger\Client\Model\BookingResponse cancelBooking($session_token, $version, $booking_id, $id)

Passenger cancels a booking

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('X-API-KEY', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-KEY', 'Bearer');
// Configure API key authorization: appId
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('X-APP-ID', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-APP-ID', 'Bearer');

$apiInstance = new Swagger\Client\Api\PassengerBookingActionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$session_token = "session_token_example"; // string | User session token
$version = "version_example"; // string | 
$booking_id = "booking_id_example"; // string | 
$id = "id_example"; // string | 

try {
    $result = $apiInstance->cancelBooking($session_token, $version, $booking_id, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PassengerBookingActionsApi->cancelBooking: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_token** | **string**| User session token |
 **version** | **string**|  |
 **booking_id** | **string**|  |
 **id** | **string**|  |

### Return type

[**\Swagger\Client\Model\BookingResponse**](../Model/BookingResponse.md)

### Authorization

[apiKey](../../README.md#apiKey), [appId](../../README.md#appId)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **requestBooking**
> \Swagger\Client\Model\BookingResponse requestBooking($session_token, $meeting, $origin_latitude, $origin_longitude, $origin_address, $version, $id, $destination_latitude, $destination_longitude, $destination_address, $observations, $pay_with_card)

Passenger requests a booking, this will eventually register a trip but with a meeting datetime specified explicitely

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('X-API-KEY', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-KEY', 'Bearer');
// Configure API key authorization: appId
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('X-APP-ID', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-APP-ID', 'Bearer');

$apiInstance = new Swagger\Client\Api\PassengerBookingActionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$session_token = "session_token_example"; // string | User session token
$meeting = "meeting_example"; // string | Date and time of the pickup meeting
$origin_latitude = "origin_latitude_example"; // string | Origin latitude
$origin_longitude = "origin_longitude_example"; // string | Origin longitude
$origin_address = "origin_address_example"; // string | Origin address
$version = "version_example"; // string | 
$id = "id_example"; // string | 
$destination_latitude = "destination_latitude_example"; // string | Destination latitude
$destination_longitude = "destination_longitude_example"; // string | Destination longitude
$destination_address = "destination_address_example"; // string | Destination address
$observations = "observations_example"; // string | Passenger observations to let to know to the driver
$pay_with_card = true; // bool | Whether the user will pay this booking with card or cash

try {
    $result = $apiInstance->requestBooking($session_token, $meeting, $origin_latitude, $origin_longitude, $origin_address, $version, $id, $destination_latitude, $destination_longitude, $destination_address, $observations, $pay_with_card);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PassengerBookingActionsApi->requestBooking: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_token** | **string**| User session token |
 **meeting** | **string**| Date and time of the pickup meeting |
 **origin_latitude** | **string**| Origin latitude |
 **origin_longitude** | **string**| Origin longitude |
 **origin_address** | **string**| Origin address |
 **version** | **string**|  |
 **id** | **string**|  |
 **destination_latitude** | **string**| Destination latitude | [optional]
 **destination_longitude** | **string**| Destination longitude | [optional]
 **destination_address** | **string**| Destination address | [optional]
 **observations** | **string**| Passenger observations to let to know to the driver | [optional]
 **pay_with_card** | **bool**| Whether the user will pay this booking with card or cash | [optional]

### Return type

[**\Swagger\Client\Model\BookingResponse**](../Model/BookingResponse.md)

### Authorization

[apiKey](../../README.md#apiKey), [appId](../../README.md#appId)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


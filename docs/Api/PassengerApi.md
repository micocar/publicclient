# Swagger\Client\PassengerApi

All URIs are relative to *http://localhost:8000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPassengerData**](PassengerApi.md#getPassengerData) | **GET** /api/public/{version}/passengers/{id}/get_data | 


# **getPassengerData**
> \Swagger\Client\Model\PassengerResponse getPassengerData($version, $id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('X-API-KEY', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-KEY', 'Bearer');
// Configure API key authorization: appId
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('X-APP-ID', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-APP-ID', 'Bearer');

$apiInstance = new Swagger\Client\Api\PassengerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$version = "version_example"; // string | 
$id = "id_example"; // string | 

try {
    $result = $apiInstance->getPassengerData($version, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PassengerApi->getPassengerData: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **string**|  |
 **id** | **string**|  |

### Return type

[**\Swagger\Client\Model\PassengerResponse**](../Model/PassengerResponse.md)

### Authorization

[apiKey](../../README.md#apiKey), [appId](../../README.md#appId)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


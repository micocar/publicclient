# Swagger\Client\GeolocationApi

All URIs are relative to *http://localhost:8000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getNearestDrivers**](GeolocationApi.md#getNearestDrivers) | **GET** /api/public/{version}/geo/get_nearest_drivers | Returns the driver positions around one location


# **getNearestDrivers**
> \Swagger\Client\Model\UserPositionResponse[] getNearestDrivers($origin_latitude, $origin_longitude, $version)

Returns the driver positions around one location

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('X-API-KEY', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-API-KEY', 'Bearer');
// Configure API key authorization: appId
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('X-APP-ID', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-APP-ID', 'Bearer');

$apiInstance = new Swagger\Client\Api\GeolocationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$origin_latitude = 8.14; // float | Latitude
$origin_longitude = 8.14; // float | Longitude
$version = "version_example"; // string | 

try {
    $result = $apiInstance->getNearestDrivers($origin_latitude, $origin_longitude, $version);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeolocationApi->getNearestDrivers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **origin_latitude** | **float**| Latitude |
 **origin_longitude** | **float**| Longitude |
 **version** | **string**|  |

### Return type

[**\Swagger\Client\Model\UserPositionResponse[]**](../Model/UserPositionResponse.md)

### Authorization

[apiKey](../../README.md#apiKey), [appId](../../README.md#appId)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

